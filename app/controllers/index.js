var AppData = require('data');

$.loginWidget = Alloy.createWidget('com.baliassist.login', null, {
    loginCallback: function(e) { 
      Alloy.Globals.authstr = Titanium.Utils.base64encode(e.username + ':' + e.password)
      Ti.App.Properties.setString('username', e.username);
      Ti.App.Properties.setString('password', e.password);
      getUserCardInfo();
    },
    remindCallback: function(e) {
      var xhr = Ti.Network.createHTTPClient({
        onload: function(e) {
          // this function is called when data is returned from the server and available for use
          // this.responseText holds the raw text return of the message (used for text/JSON)
          // this.responseXML holds any returned XML (including SOAP)
          // this.responseData holds any returned binary data

          var user_data = JSON.parse(this.responseText);
          Ti.API.debug(user_data);
        },
        onerror: function(e) {
          Ti.API.debug(e.error);
          Ti.API.debug(this.responseText);
          Ti.API.debug(this.statusText);

          // alert("Error logging in. Please try again.");

          // Ti.App.fireEvent('loginError');
        },
        timeout: 10000
      });

      xhr.open("POST", Alloy.Globals.API_BASE + '/users/reset-password');
      xhr.send({
        "user_email": e.email
      });
    }
});

// var Passbook = require("ti.passbook");

function getUserCardInfo() {
  var url = Alloy.Globals.API_BASE + "/users/me/card";

  Ti.API.debug(Alloy.Globals.API_BASE + "/users/me/card");

  if (Ti.App.Properties.hasProperty('cardViewNameLabel')) {
    $.cardViewNameLabel.setText(Ti.App.Properties.getString('cardViewNameLabel'));
  }
  if (Ti.App.Properties.hasProperty('cardViewMemberNo')) {
    $.cardViewMemberNo.setText(Ti.App.Properties.getString('cardViewMemberNo'));
  }
  if (Ti.App.Properties.hasProperty('cardViewExpiresLabel')) {
    $.cardViewExpiresLabel.setText(Ti.App.Properties.getString('cardViewExpiresLabel'));
  }
  
  if (Ti.App.Properties.hasProperty('cardViewValidSinceLabel')) {
    $.cardViewValidSinceLabel.setText(Ti.App.Properties.getString('cardViewValidSinceLabel'));
  }
  
  if (Ti.App.Properties.hasProperty('cardViewMemberType')) {
    $.cardViewMemberType.setText(Ti.App.Properties.getString('cardViewMemberType'));
  }
     
  var xhr = Ti.Network.createHTTPClient({
    onload: function(e) {
      // this function is called when data is returned from the server and available for use
      // this.responseText holds the raw text return of the message (used for text/JSON)
      // this.responseXML holds any returned XML (including SOAP)
      // this.responseData holds any returned binary data

      var user_data = JSON.parse(this.responseText);
      Ti.API.debug(user_data);

      $.loginWidget.loginContainer.animate({
        opacity: 0,
        duration: 250
      }, function() {
        Ti.App.Properties.setString('cardViewNameLabel', user_data.first_name + ' ' + user_data.last_name);
        Ti.App.Properties.setString('cardViewMemberNo', user_data.member_no);
        Ti.App.Properties.setString('cardViewExpiresLabel', user_data.expires);
        Ti.App.Properties.setString('cardViewValidSinceLabel', user_data.starts);
        Ti.App.Properties.setString('cardViewMemberType', user_data.member_type);

        $.myWindow.open();
        $.myWindow.show();
        $.cardViewNameLabel.setText(user_data.first_name + ' ' + user_data.last_name);
        $.cardViewMemberNo.setText(user_data.member_no);
        $.cardViewExpiresLabel.setText(user_data.expires);
        $.cardViewValidSinceLabel.setText(user_data.starts);
        $.cardViewMemberType.setText(user_data.member_type);
      });
    },
    onerror: function(e) {
      // $.myWindow.hide();
      $.loginWidget.open();
      Alloy.Globals.authstr = null;

      Ti.API.debug("ERR: " + e.error);
      Ti.API.debug("RespText: " + this.responseText);
      Ti.API.debug("RespText type: " + typeof this.responseText);
      Ti.API.debug("SText: " + this.statusText);

      var user_data = JSON.parse(this.responseText);

      if (user_data[0].code == "invalid_username") {
        user_data[0].message = "The e-mail address you entered is incorrect. Please type it again carefully...";
      } else if (user_data[0].message.indexOf("lostpassword") != -1) {
        user_data[0].message = "The password you entered is incorrect. Please type it again carefully or reset your password if you cannot figure it out.";
      }

      alert("Error logging in. Please try again.\n\n" + user_data[0].message);

      Ti.App.fireEvent('loginError');

      // Ti.App.Properties.setString('username');
      // Ti.App.Properties.setString('password');
    },
    timeout: 10000
  });

  xhr.open("GET", url);
  xhr.setRequestHeader('X-Authorization', Alloy.Globals.authstr);
  xhr.send();
}

function logoutHandler() {
  Alloy.Globals.authstr = null;
  Ti.App.Properties.removeProperty('username');
  Ti.App.Properties.removeProperty('password');
  Ti.App.Properties.removeProperty('cardViewExpiresLabel');
  Ti.App.Properties.removeProperty('cardViewMemberNo');
  Ti.App.Properties.removeProperty('cardViewNameLabel');

  // $.myWindow.hide();
  $.cardView.hide();
  $.loginWidget.open();
  $.myWindow.hide();
  $.loginWidget.getView().show();
}

Ti.Gesture.addEventListener('orientationchange',function(e) {
  // get current device orientation from
  // Titanium.Gesture.orientation

  // get orientation from event object
  // from e.orientation

  // Ti.Gesture.orientation should match e.orientation
  // but iOS and Android will report different values

  // two helper methods return a Boolean
  // e.source.isPortrait()
  // e.source.isLandscape()
  $.myWindow.setWidth("100%");
  $.cardView.setWidth("95%");
  $.cardView.setTop(e.source.isLandscape() ? 30 : 50);
  // $.cardView.setTransform(Ti.UI.create2DMatrix({ 
  //   rotate: 90
  // }));
});

if (Ti.App.Properties.hasProperty('username') && Ti.App.Properties.hasProperty('password')) {
  $.myWindow.open();
  Alloy.Globals.authstr = Titanium.Utils.base64encode(Ti.App.Properties.getString('username') + ':' + Ti.App.Properties.getString('password')); 
  getUserCardInfo();
} else {
  $.loginWidget.open();
}

