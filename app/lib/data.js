var loggedIn = false;
 
if (Ti.App.Properties.getString('loggedIn')) {
  loggedIn = true;
}
 
exports.isLoggedIn = function () {
  return loggedIn;
};
 
exports.login = function(username, password, callback) {
    if (username !== 'error') {
        loggedIn = true;
        Ti.App.Properties.setString('loggedIn', 1);
 
        setTimeout(function() {
            callback({ result: 'ok' }); 
        }, 1500);              
    } else {
        setTimeout(function() {
            callback({ result: 'error', msg: 'Username "error" triggers login error' });
        }, 1500);
    }       
};
 
exports.logout = function (callback) {
    loggedIn = false;
    Ti.App.Properties.removeProperty('loggedIn'); 
    callback({ result: 'ok' });
};