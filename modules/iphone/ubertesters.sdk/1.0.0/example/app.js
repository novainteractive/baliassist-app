// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white'
});

// TODO: write your module tests here
var ubertesters = require('ubertesters.sdk');
Ti.API.info("module is => " + ubertesters);

 var infoLogButton = Ti.UI.createButton({
	 title: 'Info log',
	 top: 10,
	 height: 40,
	 width: 200
     }); 
    
   infoLogButton.addEventListener('click', function(e) {
		 ubertesters.logInfo("Info log");
	 });
     
  var warningLogButton = Ti.UI.createButton({
	 title: 'Warning Log',
	 top: 60,
	 height: 40,
	 width: 200
     });
     
     warningLogButton.addEventListener('click', function(e) {
		 ubertesters.logWarn("Warning log");
	 });
 	
   var errorLogButton = Ti.UI.createButton({
	 title: 'Error Log',
	 top: 110,
	 height: 40,
	 width: 200
     }); 
     
    errorLogButton.addEventListener('click', function(e) {
		 ubertesters.logError("Error log");
	 });
    
    var takeScreenshotButton = Ti.UI.createButton({
	 title: 'Take screenshot',
	 top: 160,
	 height: 40,
	 width: 200
     });
      
    takeScreenshotButton.addEventListener('click', function(e) {
        ubertesters.takeScreenshot();
	 }); 
      
	
    var showUTPageButton = Ti.UI.createButton({
	 title: 'Show UT page',
	 top: 210,
	 height: 40,
	 width: 200
     });
     
     showUTPageButton.addEventListener('click', function(e) {
        ubertesters.showUtPage();
	 }); 

	
	 var hideUTPageButton = Ti.UI.createButton({
	 title: 'Hide UT page',
	 top: 260,
	 height: 40,
	 width: 200
     });
     
     hideUTPageButton.addEventListener('click', function(e) {
        ubertesters.hideUtPage();
	 }); 
	
 win.add(infoLogButton);
 win.add(warningLogButton);
 win.add(errorLogButton);
 win.add(takeScreenshotButton);
 win.add(showUTPageButton);
 win.add(hideUTPageButton);

